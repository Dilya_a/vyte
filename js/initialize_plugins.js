// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ СТАРТ
		
		// $(function(){
		// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
		// 	ua = navigator.userAgent,

		// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

		// 	scaleFix = function () {
		// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
		// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
		// 			document.addEventListener("gesturestart", gestureStart, false);
		// 		}
		// 	};
			
		// 	scaleFix();
		// });
		// var ua=navigator.userAgent.toLocaleLowerCase(),
		//  regV = /ipod|ipad|iphone/gi,
		//  result = ua.match(regV),
		//  userScale="";
		// if(!result){
		//  userScale=",user-scalable=0"
		// }
		// document.write('<meta name="viewport" id="myViewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

		// ============================================================
		//  window.onload = function () {
		// 	if(screen.width <= 617) {
		// 	    var mvp = document.getElementById('myViewport');
		// 	    mvp.setAttribute('content','width=617');
		// 	}
		// }
		// ============================================================

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ КОНЕЦ




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var sliderRange = $("#slider-range"),
			fancybox    = $(".fancybox"),
			page_url    = $("#page_url"),
			windowW     = $(window).width(),
			windowH     = $(window).height();


			if(sliderRange.length){
					include("plugins/slider-range/jquery-ui.js");
					include("plugins/slider-range/jquery.ui.touch-punch.min.js");				
			}
			if(fancybox.length){
					include("plugins/fancybox/jquery.fancybox.js");					
					include("plugins/fancybox/jquery.fancybox.pack.js");					
			}



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			/* ------------------------------------------------
			SLIDER-RANGE START
			------------------------------------------------ */

					if(sliderRange.length){
						sliderRange.slider({
							range: "max",
							min: 1,
							max: 10,
							value: 2,
							slide: function( event, ui ) {
								$( "#amount" ).val( ui.value );
							}
						});

						$( "#amount" ).val( sliderRange.slider( "value" ) );
					}

			/* ------------------------------------------------
			SLIDER-RANGE END
			------------------------------------------------ */

			


			/* ------------------------------------------------
			FANCYBOX START
			------------------------------------------------ */

				if(fancybox.length){
					fancybox.fancybox({
						maxWidth	: 855,
						maxHeight	: 483,
						autoSize	: false,
						padding     : false,
						'type'		: 'iframe'
					});
				}

			/* ------------------------------------------------
			FANCYBOX END
			------------------------------------------------ */





		});

		
		$(window).load(function(){

			

		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
