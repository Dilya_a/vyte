$(document).ready(function(){




		/* ------------------------------------------------
		RESPONSIVE MENU START
		------------------------------------------------ */

			$(".resp_head_menu").on("click ontouchstart", function(){
				$(".navigation_menu").toggleClass("active");
				$(".resp_head_menu").toggleClass("active");
				if($(".navigation_menu").hasClass('active')){
					$("body").css({
						overflow: 'hidden'
					});
				}
				else{
					$("body").css({
						overflow: 'inherit'
					});	
				}
			});

		/* ------------------------------------------------
		RESPONSIVE MENU END
		------------------------------------------------ */




		/* ------------------------------------------------
		FILTER ON CLICK AND CHANGE POSITION START
		------------------------------------------------ */

				//скрипт  считывает сколько ширина скрола начало

				var scrollWidth;
				function detectScrollBarWidth(){
					var div = document.createElement('div');
					div.className = "detect_scroll_width";
					document.body.appendChild(div);
					scrollWidth = div.offsetWidth - div.clientWidth;
					document.body.removeChild(div);
				}
				detectScrollBarWidth();

				//скрипт  считывает сколько ширина скрола конец

				// скрипт переставляет блок на определенной ширине в другой блок начало
				
				function filterPosition(){
		          var bodyWidth = $(window).width();
		          if(bodyWidth + scrollWidth <= 479 && $('body').hasClass('filterPosition')){
		            $('.navigation_menu').append($('.header_buttons'));
		            $('body').removeClass('filterPosition');
		          }
		          else if(bodyWidth + scrollWidth > 479 && !$('body').hasClass('filterPosition')){
		            $("#header").find('.navigation_menu').after($('.header_buttons'));
		            $('body').addClass('filterPosition');
		          }
		        } 
		        filterPosition();

				$(window).on('resize',function(){
			        setTimeout(function(){
		            	filterPosition();
		            },100);
	            });

				// скрипт переставляет блок на определенной ширине в другой блок конец

		/* ------------------------------------------------
		FILTER ON CLICK AND CHANGE POSITION END
		------------------------------------------------ */




		/* ------------------------------------------------
		TAB START
		------------------------------------------------ */

			var tabContainers = $('div.tabs_custom_box > div'); // получаем массив контейнеров
			if(tabContainers.length){

				tabContainers.hide().filter(':first').show(); // прячем все, кроме первого
				
				$('div.tabs_custom_box ul.tabs-nav a').click(function () {
					tabContainers.hide(); // прячем все табы
					tabContainers.filter(this.hash).show(); // показываем содержимое текущего
					$('div.tabs_custom_box ul.tabs-nav a').removeClass('active'); // у всех убираем класс 'active'
					$(this).addClass('active'); // текушей вкладке добавляем класс 'active'
					return false;
				}).filter(':first').click();

			}

		/* ------------------------------------------------
		TAB END
		------------------------------------------------ */



		
});


$(window).load(function(){

});